package pl.olowka.cleanarchitectureszkolenie.data.source.local;

import java.util.ArrayList;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;
import pl.olowka.cleanarchitectureszkolenie.domain.ProductDataSource;
import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 07.04.2017.
 */

public class ProductsLocalDataSource implements ProductDataSource {

    private Realm mDatabase;

    @Inject
    public ProductsLocalDataSource() {
        mDatabase = Realm.getDefaultInstance();
    }

    @Override
    public void saveProduct(Product product) {
        mDatabase.executeTransaction(realm -> realm.insertOrUpdate(product));
    }

    @Override
    public void deleteProduct(long id) {
        mDatabase.executeTransaction(realm -> {
            Product product = realm.where(Product.class).equalTo("id", id).findFirst();
            product.deleteFromRealm();
        });
    }

    @Override
    public void deleteAllBoughtProducts() {
        mDatabase.executeTransaction(realm -> {
            RealmResults<Product> results = realm.where(Product.class)
                    .equalTo("isBought", true)
                    .findAll();

            results.deleteAllFromRealm();
        });
    }

    @Override
    public void editProduct(long id, String name, String description) {
        mDatabase.executeTransaction(realm -> {
            Product product = realm.where(Product.class).equalTo("id", id).findFirst();
            product.setName(name);
            product.setDescription(description);
        });
    }

    @Override
    public void markProductAsBought(Product product) {
        if (product.isValid()) {
            mDatabase.executeTransaction(realm -> product.markAsBought());
        }
    }

    @Override
    public void markProductAsNotBought(Product product) {
        if (product.isValid()) {
            mDatabase.executeTransaction(realm -> product.markAsNotBought());
        }
    }

    @Override
    public ArrayList<Product> loadProductsList() {
        ArrayList<Product> results = new ArrayList<>();
        RealmResults<Product> products = mDatabase.where(Product.class).findAllSorted("name");
        results.addAll(products);
        return results;
    }

    @Override
    public Product loadProduct(long id) {
        return mDatabase.where(Product.class).equalTo("id", id).findFirst();
    }
}
