package pl.olowka.cleanarchitectureszkolenie.ui.products;

import java.util.ArrayList;

import javax.inject.Inject;

import pl.olowka.cleanarchitectureszkolenie.BasePresenter;
import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;
import pl.olowka.cleanarchitectureszkolenie.domain.usecase.DeleteAllBoughtProductsUseCase;
import pl.olowka.cleanarchitectureszkolenie.domain.usecase.LoadProductsListUseCase;
import pl.olowka.cleanarchitectureszkolenie.domain.usecase.MarkProductAsBoughtUseCase;
import pl.olowka.cleanarchitectureszkolenie.domain.usecase.MarkProductAsNotBoughtUseCase;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 09.06.2017.
 */

public class MainPresenter extends BasePresenter<MainContract.UI>
        implements MainContract.Presenter {

    private LoadProductsListUseCase mLoadProductsListUseCase;
    private MarkProductAsBoughtUseCase mMarkProductAsBoughtUseCase;
    private MarkProductAsNotBoughtUseCase mMarkProductAsNotBoughtUseCase;
    private DeleteAllBoughtProductsUseCase mDeleteAllBoughtProductsUseCase;

    @Inject
    public MainPresenter(LoadProductsListUseCase loadProductsListUseCase,
                         MarkProductAsBoughtUseCase markProductAsBoughtUseCase,
                         MarkProductAsNotBoughtUseCase markProductAsNotBoughtUseCase,
                         DeleteAllBoughtProductsUseCase deleteAllBoughtProductsUseCase) {

        mLoadProductsListUseCase = loadProductsListUseCase;
        mMarkProductAsBoughtUseCase = markProductAsBoughtUseCase;
        mMarkProductAsNotBoughtUseCase = markProductAsNotBoughtUseCase;
        mDeleteAllBoughtProductsUseCase = deleteAllBoughtProductsUseCase;
    }

    @Override
    public void attach(MainContract.UI ui) {
        super.attach(ui);
        loadProducts();
    }

    @Override
    public void loadProducts() {
        ArrayList<Product> results = mLoadProductsListUseCase.execute(null);
        execute(new ShowProductsCmd(results));
        if (results.size() > 0) {
            execute(new HideEmptyDataInfoCmd());
        } else {
            execute(new ShowEmptyDataInfoCmd());
        }
    }

    @Override
    public void changeProductBoughtStatus(Product product, boolean isBought) {
        if (isBought) {
            mMarkProductAsBoughtUseCase.execute(product);
        } else {
            mMarkProductAsNotBoughtUseCase.execute(product);
        }
        execute(new ShowBoughtInfoCmd(product.getName(), isBought));
    }

    @Override
    public void deleteAllBoughtProducts() {
        mDeleteAllBoughtProductsUseCase.execute(null);
        loadProducts();
    }

    private static class ShowEmptyDataInfoCmd implements UICommand<MainContract.UI> {

        @Override
        public void execute(MainContract.UI ui) {
            ui.showEmptyDataInfo();
        }
    }

    private static class HideEmptyDataInfoCmd implements UICommand<MainContract.UI> {

        @Override
        public void execute(MainContract.UI ui) {
            ui.hideEmptyDataInfo();
        }
    }

    private static class ShowProductsCmd implements UICommand<MainContract.UI> {
        private ArrayList<Product> results;

        public ShowProductsCmd(ArrayList<Product> results) {
            this.results = results;
        }

        @Override
        public void execute(MainContract.UI ui) {
            ui.showProducts(results);
        }
    }

    private static class ShowBoughtInfoCmd implements UICommand<MainContract.UI> {
        private String name;
        private boolean isBought;

        public ShowBoughtInfoCmd(String name, boolean isBought) {
            this.name = name;
            this.isBought = isBought;
        }

        @Override
        public void execute(MainContract.UI ui) {
            ui.showBoughtInfo(name, isBought);
        }
    }
}
