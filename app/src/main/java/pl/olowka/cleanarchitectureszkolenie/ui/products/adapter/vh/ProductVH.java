package pl.olowka.cleanarchitectureszkolenie.ui.products.adapter.vh;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.olowka.cleanarchitectureszkolenie.R;
import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;
import pl.olowka.cleanarchitectureszkolenie.ui.products.adapter.ProductsAdapter;

/**
 * Created by Esiek on 19.06.2017.
 */

public class ProductVH extends RecyclerView.ViewHolder {

    @BindView(R.id.name)
    TextView mName;

    @BindView(R.id.description)
    TextView mDescription;

    @BindView(R.id.checkbox)
    CheckBox mBoughtStatus;

    private ProductsAdapter.ProductItemListener mItemListener;

    public ProductVH(View itemView, ProductsAdapter.ProductItemListener itemListener) {
        super(itemView);
        mItemListener = itemListener;
        ButterKnife.bind(this, itemView);
    }

    public void populateView(Product product) {
        mName.setText(product.getName());
        mDescription.setText(product.getDescription());
        mBoughtStatus.setOnCheckedChangeListener(null);
        mBoughtStatus.setChecked(product.isBought());
        mBoughtStatus.setOnCheckedChangeListener(
                (compoundButton, b) -> mItemListener.onBoughtStatusChange(product, b));

        itemView.setOnClickListener(
                view -> mItemListener.onProductClick(product));
    }
}
