package pl.olowka.cleanarchitectureszkolenie.ui.addeditproduct;

import javax.inject.Inject;

import pl.olowka.cleanarchitectureszkolenie.BasePresenter;
import pl.olowka.cleanarchitectureszkolenie.domain.ProductFactory;
import pl.olowka.cleanarchitectureszkolenie.domain.exception.ProductInvalidException;
import pl.olowka.cleanarchitectureszkolenie.domain.model.EditedProduct;
import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;
import pl.olowka.cleanarchitectureszkolenie.domain.usecase.AddProductUseCase;
import pl.olowka.cleanarchitectureszkolenie.domain.usecase.DeleteProductUseCase;
import pl.olowka.cleanarchitectureszkolenie.domain.usecase.EditProductUseCase;
import pl.olowka.cleanarchitectureszkolenie.domain.usecase.LoadProductUseCase;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 09.06.2017.
 */

public class AddEditProductPresenter extends BasePresenter<AddEditProductContract.UI>
        implements AddEditProductContract.Presenter {

    private AddProductUseCase mSaveProductUseCase;
    private LoadProductUseCase mLoadProductUseCase;
    private EditProductUseCase mEditProductUseCase;
    private DeleteProductUseCase mDeleteProductUseCase;
    private ProductFactory mProductFactory;

    @Inject
    public AddEditProductPresenter(AddProductUseCase saveProductUseCase,
                                   LoadProductUseCase loadProductUseCase,
                                   EditProductUseCase editProductUseCase,
                                   DeleteProductUseCase deleteProductUseCase,
                                   ProductFactory productFactory) {

        mSaveProductUseCase = saveProductUseCase;
        mLoadProductUseCase = loadProductUseCase;
        mEditProductUseCase = editProductUseCase;
        mDeleteProductUseCase = deleteProductUseCase;
        mProductFactory = productFactory;
    }

    @Override
    public void createProduct(String name, String description) {
        try {
            Product product = mProductFactory.createProduct(System.currentTimeMillis(), name, description);

            mSaveProductUseCase.execute(product);
            execute(new ProductSavedSuccessfulCmd());
        } catch (ProductInvalidException exception) {
            execute(new ShowErrorMessageCmd(exception.getMessage()));
        }
    }

    @Override
    public void editProduct(long id, String name, String description) {
        EditedProduct editedProduct = new EditedProduct(id, name, description);
        mEditProductUseCase.execute(editedProduct);
        execute(new ProductSavedSuccessfulCmd());
    }

    @Override
    public void loadProduct(long id) {
        Product product = mLoadProductUseCase.execute(id);
        execute(new SetProductCmd(product));
    }

    @Override
    public void deleteProduct(long id) {
        mDeleteProductUseCase.execute(id);
        execute(new ProductDeletedSuccessfulCmd());
    }

    private static class ProductDeletedSuccessfulCmd implements UICommand<AddEditProductContract.UI> {

        @Override
        public void execute(AddEditProductContract.UI ui) {
            ui.productDeletedSuccessful();
        }
    }

    private static class ProductSavedSuccessfulCmd implements UICommand<AddEditProductContract.UI> {

        @Override
        public void execute(AddEditProductContract.UI ui) {
            ui.productSavedSuccessful();
        }
    }

    private static class SetProductCmd implements UICommand<AddEditProductContract.UI> {

        private Product product;

        public SetProductCmd(Product product) {
            this.product = product;
        }

        @Override
        public void execute(AddEditProductContract.UI ui) {
            ui.setProductComponents(product);
        }
    }

    private static class ShowErrorMessageCmd implements UICommand<AddEditProductContract.UI> {

        private String message;

        public ShowErrorMessageCmd(String message) {
            this.message = message;
        }

        @Override
        public void execute(AddEditProductContract.UI ui) {
            ui.showErrorMessage(message);
        }
    }
}
