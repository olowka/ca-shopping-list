package pl.olowka.cleanarchitectureszkolenie.ui.products;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import dagger.android.support.DaggerAppCompatActivity;
import pl.olowka.cleanarchitectureszkolenie.R;
import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;
import pl.olowka.cleanarchitectureszkolenie.ui.addeditproduct.AddEditProductActivity;
import pl.olowka.cleanarchitectureszkolenie.ui.products.adapter.ProductsAdapter;

public class MainActivity extends DaggerAppCompatActivity implements MainContract.UI {

    @BindView(R.id.root)
    CoordinatorLayout mRoot;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.empty_data_container)
    FrameLayout mEmptyDataContainer;

    @Inject
    MainPresenter mPresenter;

    private ProductsAdapter mAdapter;
    private DividerItemDecoration mItemDivider;

    private ProductsAdapter.ProductItemListener mProductItemListener =
            new ProductsAdapter.ProductItemListener() {
                @Override
                public void onProductClick(Product product) {
                    editProduct(product.getId());
                }

                @Override
                public void onBoughtStatusChange(Product product, boolean isBought) {
                    mPresenter.changeProductBoughtStatus(product, isBought);
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupToolbar();
        initializeRecyclerViewComponents();
        setupRecyclerView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int clickedItemId = item.getItemId();
        switch (clickedItemId) {
            case R.id.delete:
                deleteBoughtProducts();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteBoughtProducts() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.delete_dialog_title)
                .setMessage(R.string.delete_dialog_msg)
                .setNegativeButton(R.string.no, (dialog, i) -> dialog.dismiss())
                .setPositiveButton(R.string.yes, (dialog, i) -> {
                    mPresenter.deleteAllBoughtProducts();
                    dialog.dismiss();
                })
                .show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.attach(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.detach();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

    private void initializeRecyclerViewComponents() {
        mAdapter = new ProductsAdapter(mProductItemListener);
        mItemDivider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
    }

    private void setupRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(mItemDivider);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void editProduct(long id) {
        Intent intent = new Intent(this, AddEditProductActivity.class);
        intent.putExtra(AddEditProductActivity.EDIT_MODE_KEY, true);
        intent.putExtra(AddEditProductActivity.PRODUCT_ID_KEY, id);
        startActivity(intent);
    }

    @Override
    public void showProducts(ArrayList<Product> products) {
        mAdapter.setData(products);
    }

    @Override
    public void showBoughtInfo(String name, boolean isBought) {
        CharSequence msg;
        if (isBought) {
            msg = TextUtils.concat(name, ": kupiony");
        } else {
            msg = TextUtils.concat(name, ": niekupiony");
        }

        Snackbar.make(mRoot, msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showEmptyDataInfo() {
        mRecyclerView.setVisibility(View.INVISIBLE);
        mEmptyDataContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyDataInfo() {
        mRecyclerView.setVisibility(View.VISIBLE);
        mEmptyDataContainer.setVisibility(View.GONE);
    }

    @OnClick(R.id.fab)
    public void fabButtonClicked() {
        Intent intent = new Intent(this, AddEditProductActivity.class);
        startActivity(intent);
    }
}
