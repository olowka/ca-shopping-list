package pl.olowka.cleanarchitectureszkolenie.ui.addeditproduct;

import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 09.06.2017.
 */

public interface AddEditProductContract {

    interface UI {
        void setProductComponents(Product product);
        void productSavedSuccessful();
        void productDeletedSuccessful();
        void showErrorMessage(String message);
    }

    interface Presenter {
        void createProduct(String name, String description);
        void editProduct(long id, String name, String description);
        void loadProduct(long id);
        void deleteProduct(long id);
    }
}
