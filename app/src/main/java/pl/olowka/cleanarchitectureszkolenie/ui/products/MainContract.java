package pl.olowka.cleanarchitectureszkolenie.ui.products;

import java.util.ArrayList;

import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 09.06.2017.
 */

public interface MainContract {

    interface UI {
        void showProducts(ArrayList<Product> products);
        void showBoughtInfo(String name, boolean isBought);
        void showEmptyDataInfo();
        void hideEmptyDataInfo();
    }

    interface Presenter {
        void loadProducts();
        void changeProductBoughtStatus(Product product, boolean isBought);
        void deleteAllBoughtProducts();
    }
}
