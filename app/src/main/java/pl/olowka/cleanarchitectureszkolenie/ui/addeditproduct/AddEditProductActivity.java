package pl.olowka.cleanarchitectureszkolenie.ui.addeditproduct;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import dagger.android.support.DaggerAppCompatActivity;
import pl.olowka.cleanarchitectureszkolenie.R;
import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 09.06.2017.
 */

public class AddEditProductActivity extends DaggerAppCompatActivity
        implements AddEditProductContract.UI {

    public static final String EDIT_MODE_KEY = "edit_mode_key";
    public static final String PRODUCT_ID_KEY = "product_id_key";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.name)
    EditText mNameInput;

    @BindView(R.id.description)
    EditText mDescriptionInput;

    @BindView(R.id.save)
    AppCompatButton mSaveButton;

    @Inject
    AddEditProductPresenter mPresenter;

    private boolean mIsEditMode;
    private long mProductId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_product);
        ButterKnife.bind(this);
        setupToolbar();
        initializeArgs();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mIsEditMode) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.menu_edit, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int clickedItemId = item.getItemId();
        switch (clickedItemId) {
            case R.id.delete:
                deleteProduct();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.attach(this);
        if (mIsEditMode) {
            mPresenter.loadProduct(mProductId);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.detach();
    }

    @OnClick(R.id.save)
    public void saveButtonClicked() {
        String name = mNameInput.getText().toString();
        String description = mDescriptionInput.getText().toString();
        if(mIsEditMode) {
            mPresenter.editProduct(mProductId, name, description);
        } else {
            mPresenter.createProduct(name, description);
        }
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public void initializeArgs() {
        if (getIntent() != null) {
            mIsEditMode = getIntent().getBooleanExtra(EDIT_MODE_KEY, false);
            mProductId = getIntent().getLongExtra(PRODUCT_ID_KEY, -1);
        }
    }

    private void deleteProduct() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.delete_dialog_title)
                .setMessage(R.string.delete_dialog_msg)
                .setNegativeButton(R.string.no, (dialog, i) -> dialog.dismiss())
                .setPositiveButton(R.string.yes, (dialog, i) -> {
                    mPresenter.deleteProduct(mProductId);
                    dialog.dismiss();
                })
                .show();
    }

    @Override
    public void setProductComponents(Product product) {
        mNameInput.setText(product.getName());
        mDescriptionInput.setText(product.getDescription());
        mSaveButton.setText(getString(R.string.change));
    }

    @Override
    public void productSavedSuccessful() {
        finish();
    }

    @Override
    public void productDeletedSuccessful() {
        finish();
    }

    @Override
    public void showErrorMessage(String message) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.product_error)
                .setMessage(message)
                .setPositiveButton(R.string.ok, (dialog, i) -> dialog.dismiss())
                .show();
    }
}
