package pl.olowka.cleanarchitectureszkolenie.ui.products.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import pl.olowka.cleanarchitectureszkolenie.R;
import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;
import pl.olowka.cleanarchitectureszkolenie.ui.products.adapter.vh.ProductVH;

/**
 * Created by Esiek on 19.06.2017.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductVH> {

    private ArrayList<Product> mData;
    private ProductItemListener mProductItemListener;

    public ProductsAdapter(ProductItemListener productItemListener) {
        mData = new ArrayList<>();
        mProductItemListener = productItemListener;
    }

    public void setData(ArrayList<Product> products) {
        mData.clear();
        mData.addAll(products);
        notifyDataSetChanged();
    }

    @Override
    public ProductVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vh_product_item, parent, false);

        return new ProductVH(view, mProductItemListener);
    }

    @Override
    public void onBindViewHolder(ProductVH holder, int position) {
        holder.populateView(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public interface ProductItemListener {
        void onProductClick(Product product);
        void onBoughtStatusChange(Product product, boolean isBought);
    }
}
