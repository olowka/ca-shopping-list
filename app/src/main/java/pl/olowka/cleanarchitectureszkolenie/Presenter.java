package pl.olowka.cleanarchitectureszkolenie;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 16/03/2017.
 */

public interface Presenter<UI> {

    void attach(UI ui);

    void detach();

}
