package pl.olowka.cleanarchitectureszkolenie.domain.validators;

import pl.olowka.cleanarchitectureszkolenie.domain.exception.ProductInvalidException;
import pl.olowka.cleanarchitectureszkolenie.domain.ProductValidator;
import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;

/**
 * @author Piotr Winiarski <piotr.winiarski@agora.pl>
 */

public class EmptyProductNameValidator implements ProductValidator {

    @Override
    public void validate(Product product) {
        if (product.getName() == null || product.getName().isEmpty()) {
            throw new ProductInvalidException("Nazwa produktu nie może być pusta.");
        }
    }
}
