package pl.olowka.cleanarchitectureszkolenie.domain;

import javax.inject.Inject;

import pl.olowka.cleanarchitectureszkolenie.domain.exception.ProductInvalidException;
import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;

/**
 * @author Piotr Winiarski <piotr.winiarski@agora.pl>
 */

public class ProductFactory {
    private final ProductValidator validator;

    @Inject
    public ProductFactory(ProductValidator validator) {
        this.validator = validator;
    }

    public Product createProduct(long id, String name, String description) throws ProductInvalidException {
        Product product = new Product(id, name, description);
        validator.validate(product);
        return product;
    }
}
