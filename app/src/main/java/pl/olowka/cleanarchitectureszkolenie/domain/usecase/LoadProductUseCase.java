package pl.olowka.cleanarchitectureszkolenie.domain.usecase;

import javax.inject.Inject;

import pl.olowka.cleanarchitectureszkolenie.domain.ProductDataSource;
import pl.olowka.cleanarchitectureszkolenie.domain.UseCase;
import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 09.06.2017.
 */

public class LoadProductUseCase implements UseCase<Product, Long> {

    private ProductDataSource mProductDataSource;

    @Inject
    public LoadProductUseCase(ProductDataSource productDataSource) {
        mProductDataSource = productDataSource;
    }

    @Override
    public Product execute(Long id) {
        return mProductDataSource.loadProduct(id);
    }
}
