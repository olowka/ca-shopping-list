package pl.olowka.cleanarchitectureszkolenie.domain.usecase;

import javax.inject.Inject;

import pl.olowka.cleanarchitectureszkolenie.domain.ProductDataSource;
import pl.olowka.cleanarchitectureszkolenie.domain.UseCase;
import pl.olowka.cleanarchitectureszkolenie.domain.model.EditedProduct;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 09.06.2017.
 */

public class EditProductUseCase implements UseCase<Void, EditedProduct> {

    private ProductDataSource mProductDataSource;

    @Inject
    public EditProductUseCase(ProductDataSource productDataSource) {
        mProductDataSource = productDataSource;
    }

    @Override
    public Void execute(EditedProduct editedProduct) {
        mProductDataSource.editProduct(
                editedProduct.getId(),
                editedProduct.getName(),
                editedProduct.getDescription());
        return null;
    }
}
