package pl.olowka.cleanarchitectureszkolenie.domain;

import java.util.ArrayList;

import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 19.06.2017.
 */

public interface ProductDataSource {

    void saveProduct(Product product);
    void deleteProduct(long id);
    void deleteAllBoughtProducts();
    void editProduct(long id, String name, String description);
    void markProductAsBought(Product product);
    void markProductAsNotBought(Product product);
    ArrayList<Product> loadProductsList();
    Product loadProduct(long id);
}
