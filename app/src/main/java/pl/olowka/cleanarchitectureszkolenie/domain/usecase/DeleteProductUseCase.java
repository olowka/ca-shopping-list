package pl.olowka.cleanarchitectureszkolenie.domain.usecase;

import javax.inject.Inject;

import pl.olowka.cleanarchitectureszkolenie.domain.ProductDataSource;
import pl.olowka.cleanarchitectureszkolenie.domain.UseCase;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 09.06.2017.
 */

public class DeleteProductUseCase implements UseCase<Void, Long> {

    private ProductDataSource mProductDataSource;

    @Inject
    public DeleteProductUseCase(ProductDataSource productDataSource) {
        mProductDataSource = productDataSource;
    }

    @Override
    public Void execute(Long id) {
        mProductDataSource.deleteProduct(id);
        return null;
    }
}
