package pl.olowka.cleanarchitectureszkolenie.domain;

import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;

/**
 * @author Piotr Winiarski <piotr.winiarski@agora.pl>
 */

public interface ProductValidator {

    void validate(Product product);
}
