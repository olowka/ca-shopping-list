package pl.olowka.cleanarchitectureszkolenie.domain.exception;

/**
 * @author Piotr Winiarski <piotr.winiarski@agora.pl>
 */

public class ProductInvalidException extends RuntimeException {

    public ProductInvalidException(String message) {
        super(message);
    }
}
