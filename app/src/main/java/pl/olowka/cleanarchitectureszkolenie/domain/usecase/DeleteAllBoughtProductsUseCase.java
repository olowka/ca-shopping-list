package pl.olowka.cleanarchitectureszkolenie.domain.usecase;

import java.util.ArrayList;

import javax.inject.Inject;

import pl.olowka.cleanarchitectureszkolenie.domain.ProductDataSource;
import pl.olowka.cleanarchitectureszkolenie.domain.UseCase;
import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 09.06.2017.
 */

public class DeleteAllBoughtProductsUseCase implements UseCase<ArrayList<Product>, Void> {

    private ProductDataSource mProductDataSource;

    @Inject
    public DeleteAllBoughtProductsUseCase(ProductDataSource productDataSource) {
        mProductDataSource = productDataSource;
    }

    @Override
    public ArrayList<Product> execute(Void aVoid) {
        mProductDataSource.deleteAllBoughtProducts();
        return null;
    }
}
