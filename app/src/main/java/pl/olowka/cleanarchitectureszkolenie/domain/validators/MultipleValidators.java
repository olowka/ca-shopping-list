package pl.olowka.cleanarchitectureszkolenie.domain.validators;

import pl.olowka.cleanarchitectureszkolenie.domain.ProductValidator;
import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;

/**
 * @author Piotr Winiarski <piotr.winiarski@agora.pl>
 */

public class MultipleValidators implements ProductValidator {

    private final ProductValidator[] validators;

    public MultipleValidators(ProductValidator... validators) {
        this.validators = validators;
    }

    @Override
    public void validate(Product product) {
        for (ProductValidator validator : validators) {
            validator.validate(product);
        }
    }
}
