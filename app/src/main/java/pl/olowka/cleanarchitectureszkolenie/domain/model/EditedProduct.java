package pl.olowka.cleanarchitectureszkolenie.domain.model;

/**
 * Created by Esiek on 21.06.2017.
 */

public class EditedProduct {

    private long id;
    private String name;
    private String description;

    public EditedProduct(long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
