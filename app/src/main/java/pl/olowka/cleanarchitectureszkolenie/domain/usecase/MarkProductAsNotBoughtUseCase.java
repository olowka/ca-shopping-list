package pl.olowka.cleanarchitectureszkolenie.domain.usecase;

import javax.inject.Inject;

import pl.olowka.cleanarchitectureszkolenie.domain.ProductDataSource;
import pl.olowka.cleanarchitectureszkolenie.domain.UseCase;
import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 09.06.2017.
 */

public class MarkProductAsNotBoughtUseCase implements UseCase<Void, Product> {

    private ProductDataSource mProductDataSource;

    @Inject
    public MarkProductAsNotBoughtUseCase(ProductDataSource productDataSource) {
        mProductDataSource = productDataSource;
    }

    @Override
    public Void execute(Product product) {
        mProductDataSource.markProductAsNotBought(product);
        return null;
    }
}
