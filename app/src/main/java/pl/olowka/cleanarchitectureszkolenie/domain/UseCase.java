package pl.olowka.cleanarchitectureszkolenie.domain;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 19.06.2017.
 */

public interface UseCase<Result, Argument> {
    Result execute(Argument argument);
}
