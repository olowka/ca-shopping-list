package pl.olowka.cleanarchitectureszkolenie.domain.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 09.06.2017.
 */

public class Product extends RealmObject {

    @PrimaryKey
    private long id;
    private String name;
    private String description;
    private boolean isBought;

    public Product() {
    }

    public Product(long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isBought() {
        return isBought;
    }

    public void markAsBought() {
        isBought = true;
    }

    public void markAsNotBought() {
        isBought = false;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
