package pl.olowka.cleanarchitectureszkolenie.domain.validators;

import pl.olowka.cleanarchitectureszkolenie.domain.exception.ProductInvalidException;
import pl.olowka.cleanarchitectureszkolenie.domain.ProductValidator;
import pl.olowka.cleanarchitectureszkolenie.domain.model.Product;

/**
 * @author Piotr Winiarski <piotr.winiarski@agora.pl>
 */

public class TooShortProductNameValidator implements ProductValidator {

    @Override
    public void validate(Product product) {
        if (product.getName().length() <= 2) {
            throw new ProductInvalidException("Nazwa produktu musi mieć co najmniej 3 znaki.");
        }
    }
}
