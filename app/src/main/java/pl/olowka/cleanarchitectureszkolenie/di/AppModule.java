package pl.olowka.cleanarchitectureszkolenie.di;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import pl.olowka.cleanarchitectureszkolenie.App;
import pl.olowka.cleanarchitectureszkolenie.domain.ProductFactory;
import pl.olowka.cleanarchitectureszkolenie.domain.ProductValidator;
import pl.olowka.cleanarchitectureszkolenie.domain.validators.EmptyProductNameValidator;
import pl.olowka.cleanarchitectureszkolenie.domain.validators.MultipleValidators;
import pl.olowka.cleanarchitectureszkolenie.domain.validators.TooShortProductNameValidator;

/**
 * Created by grzegorzo on 16.12.2015.
 */

@Module(subcomponents = {
        MainActivitySubComponent.class,
        AddEditProductSubComponent.class
})

public class AppModule {

    @Provides
    Context provideContext(App application) {
        return application.getApplicationContext();
    }

    @Provides
    ProductFactory provideProductFactory(ProductValidator validator) {
        return new ProductFactory(validator);
    }

    @Provides
    ProductValidator provideProductValidator() {
        return new MultipleValidators(new EmptyProductNameValidator(), new TooShortProductNameValidator());
    }
}
