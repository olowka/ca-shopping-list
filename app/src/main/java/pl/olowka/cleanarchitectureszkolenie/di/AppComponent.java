package pl.olowka.cleanarchitectureszkolenie.di;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;
import pl.olowka.cleanarchitectureszkolenie.App;

/**
 * Created by grzegorzo on 18.12.2015.
 */

@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        BuildersModule.class
})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(App application);
        AppComponent build();
    }

    void inject(App application);
}
