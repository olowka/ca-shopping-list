package pl.olowka.cleanarchitectureszkolenie.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import pl.olowka.cleanarchitectureszkolenie.ui.addeditproduct.AddEditProductActivity;
import pl.olowka.cleanarchitectureszkolenie.ui.products.MainActivity;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 09.06.2017.
 */

@Subcomponent
public interface AddEditProductSubComponent extends AndroidInjector<AddEditProductActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<AddEditProductActivity> {

    }
}
