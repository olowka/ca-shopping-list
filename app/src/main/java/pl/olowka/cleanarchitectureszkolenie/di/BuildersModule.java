package pl.olowka.cleanarchitectureszkolenie.di;

import android.app.Activity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;
import pl.olowka.cleanarchitectureszkolenie.data.source.local.ProductsLocalDataSource;
import pl.olowka.cleanarchitectureszkolenie.domain.ProductDataSource;
import pl.olowka.cleanarchitectureszkolenie.ui.addeditproduct.AddEditProductActivity;
import pl.olowka.cleanarchitectureszkolenie.ui.products.MainActivity;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 09.06.2017.
 */

@Module
public abstract class BuildersModule {

    @Binds
    @IntoMap
    @ActivityKey(MainActivity.class)
    abstract AndroidInjector.Factory<? extends Activity>
    bindMainActivityInjectorFactory(MainActivitySubComponent.Builder builder);

    @Binds
    @IntoMap
    @ActivityKey(AddEditProductActivity.class)
    abstract AndroidInjector.Factory<? extends Activity>
    bindAddEditProductActivityInjectorFactory(AddEditProductSubComponent.Builder builder);

    @Binds
    abstract ProductDataSource productRepository(ProductsLocalDataSource dataSource);
}
