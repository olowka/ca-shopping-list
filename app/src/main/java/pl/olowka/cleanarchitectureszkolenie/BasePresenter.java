package pl.olowka.cleanarchitectureszkolenie;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Grzegorz Olowka <grzegorz.olowka@agora.pl> on 16/03/2017.
 */

public abstract class BasePresenter<UI> implements Presenter<UI> {

    private Queue<UICommand<UI>> mCommandQueue = new LinkedList<>();
    private UI mUi;

    @Override
    public void attach(UI ui) {
        this.mUi = ui;

        UICommand<UI> cmd = mCommandQueue.poll();
        while (cmd != null) {
            cmd.execute(this.mUi);
            cmd = mCommandQueue.poll();
        }
    }

    @Override
    public void detach() {
        this.mUi = null;
    }

    public void execute(UICommand<UI> cmd) {
        if (this.mUi != null) {
            cmd.execute(this.mUi);
        } else {
            mCommandQueue.offer(cmd);
        }
    }

    public interface UICommand<UI> {
        void execute(UI ui);
    }
}
