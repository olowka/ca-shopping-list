package pl.olowka.cleanarchitectureszkolenie.domain.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Esiek on 04.07.2017.
 */
public class ProductTest {

    @Test
    public void testShouldBeBoughtWhenMarkedAsBought() throws Exception {
        Product product = new Product(0, "Chleb", "Razowy");
        product.markAsBought();
        assertEquals(true, product.isBought());
    }

}